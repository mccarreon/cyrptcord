import {Message} from 'discord.js';
export enum ICommandHandlerArgType {
    ANY,
    NUMBER,
    INTEGER,
    FLOAT
}

export interface ICommandHandler {
    command: string,
    args: ICommandHandlerArg[],
    HandleCommand(args: ICommandHandlerData): void
}

export interface ICommandHandlerArg {
    position: number,
    argType: ICommandHandlerArgType,

}

export interface ICommandHandlerData {
    message: Message
    args: any[]
}