export interface IModule {
    identifier: string,
    name: string,
    author: string,
    dependencies: IModule[],
    optionalDependencies: IModule[],
    preInit(): void,
    init(dependencies?: IModule[], optionalDependencies?: IModule[]): void,
    postInit(): void
}