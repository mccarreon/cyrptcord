import {Client, Message} from 'discord.js';
import { ICommandHandler, ICommandHandlerArg, ICommandHandlerArgType } from './interfaces/commandhandler';
import { IModule } from './interfaces/module';
import * as fs from 'fs';
import { IConfig } from './interfaces/config';

export class Controller {
    private client: Client;
    private config: IConfig;
    private commandHandlers: Map<string, ICommandHandler>;
    private modules: Map<string, IModule>;

    private MODULE_DIRECTORY: string = "modules/";

    constructor() {
        this.config = require(process.env.CONFIG_LOCATION || '../config.json');
        this.client = new Client();
        this.commandHandlers = new Map<string, ICommandHandler>();
    }

    private getDiscordAPIToken(config: {token: string}): string {
        return process.env.DISCORD_TOKEN || config.token;
    }
    
    private clientLogin(): Promise<Error> {
        return new Promise((resolve, reject) => {
            const loginFailed = (err: Error) => {console.error("An error occurred while logging in."); reject(err);}
            const loginSucceeded = () => {
                this.client.off('error', loginFailed);
                resolve(null);
            }
            this.client.login(this.getDiscordAPIToken(this.config)).catch(loginFailed).then(loginSucceeded);
        })
    }

    private validateArg(argValidation: ICommandHandlerArg, arg: string): boolean {
        switch(argValidation.argType) {
            case ICommandHandlerArgType.ANY: return true;
            case ICommandHandlerArgType.NUMBER: return !isNaN(Number(arg));
            case ICommandHandlerArgType.INTEGER: return !isNaN(parseInt(arg));
            case ICommandHandlerArgType.FLOAT: return !isNaN(parseFloat(arg));
        }
    }

    private messageHandler(message: Message) {
        if(!message.content.startsWith(this.config.prefix) || message.author.bot) {
            return;
        }
        const commandComponents: string[] = message.content.substr(this.config.prefix.length).split(' ');
        const command: string = commandComponents[0];
        if(!this.commandHandlers.has(command)) {
            return;
        }
        const commandHandler : ICommandHandler = this.commandHandlers.get(command);
        const args: string[] = commandComponents.slice(1);
        const validated: boolean = commandHandler.args.map(arg => this.validateArg(arg, args[arg.position])).reduce((prev, curr) => curr && prev, true);
        if(!validated) {
            message.channel.send("Command failed validation");
            return;
        }
        
    }

    //TODO: add config -> env var mapping json to automatically pull from env variables if they exist and overwrite in memory config
    private parseConfig(config: IConfig) {
        
    }

    private initializeClientListeners() {
        this.client.on('message', this.messageHandler);
        this.client.on('disconnect', () => console.log("Bot disconnected"));
    }

    private loadModules() {
        this.modules = fs.readdirSync(this.MODULE_DIRECTORY)
        .filter(fileName => fileName.endsWith(".js"))
        .reduce<Map<string, IModule>>((mapping, newFile) => {
            const m: IModule = require(`${this.MODULE_DIRECTORY}${newFile}`);
            mapping.set(m.identifier, m);
            return mapping;
        }, new Map<string, IModule>())
    }

    public async start() {
        try {
            await this.clientLogin();
        }
        catch(e) {
            console.error("Error in bot startup");
            console.log(e);
        }

    }

    public registerCommandHandler(commandHandler: ICommandHandler) {
        if(this.commandHandlers.has(commandHandler.command)) {
            console.error(`A command handler for the command "${commandHandler.command}" has already been registered!`);
            return;
        }
        this.commandHandlers.set(commandHandler.command, commandHandler);
    }
}