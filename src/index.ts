import { Controller } from "./controller";

function main() {
    let controller: Controller = new Controller();
    controller.start();
}
main();